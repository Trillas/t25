package crudInformatica.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import crudInformatica.Dao.IArticulosDao;
import crudInformatica.Dto.Articulos;
@Service
public class ArticulosServiceImpl implements IArticulosService{
	
	@Autowired
	IArticulosDao iArticulosDAO;
	
	@Override
	public List<Articulos> listarArticuloss() {
		
		return iArticulosDAO.findAll();
	}

	@Override
	public Articulos guardarArticulos(Articulos Articulos) {
		
		return iArticulosDAO.save(Articulos);
	}

	@Override
	public Articulos ArticulosXID(Long id) {
		
		return iArticulosDAO.findById(id).get();
	}

	@Override
	public Articulos actualizarArticulos(Articulos Articulos) {
		
		return iArticulosDAO.save(Articulos);
	}

	@Override
	public void eliminarArticulos(Long id) {
		
		iArticulosDAO.deleteById(id);
		
	}
}
