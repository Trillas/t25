package crudInformatica.Dao;
import org.springframework.data.jpa.repository.JpaRepository;
import crudInformatica.Dto.Articulos;


public interface IArticulosDao extends JpaRepository<Articulos, Long> {

}
