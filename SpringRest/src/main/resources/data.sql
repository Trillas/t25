DROP table IF EXISTS articulos;
DROP table IF EXISTS fabricante;

create table fabricante(
    id Long not null auto_increment primary key,
    nombre varchar(250) null
);
create table articulos(
    id Long not null auto_increment primary key,
    nombre varchar(250) null,
    precio int null,
    fabricante int,
    FOREIGN KEY (fabricante) REFERENCES fabricante(id)
);

insert into fabricante (nombre)values('Bic');
insert into fabricante (nombre)values('Valisa');
insert into articulos (nombre, precio, fabricante)values('boli', 3, 1);
insert into articulos (nombre, precio, fabricante)values('vaso', 1, 2);