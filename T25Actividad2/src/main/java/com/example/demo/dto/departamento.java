package com.example.demo.dto;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "departamentos")
public class departamento {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(name = "nombre")
	private String nombre;
	@Column(name = "presupuesto")
	private Integer presupuesto;
	
	@OneToMany
	@JoinColumn(name = "id")
	private List<empleado> empleado;
	
	//Constructores
	
	public departamento() {
		
	}

	
	/**
	 * @param id
	 * @param nombre
	 * @param presupuesto
	 */
	
	public departamento(Integer id, String nombre, Integer presupuesto) {
		//super();
		this.id = id;
		this.nombre = nombre;
		this.presupuesto = presupuesto;
	}

	//Getters y Setters

	/**
	 * 
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * 
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * 
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * 
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * 
	 * @return the presupuesto
	 */
	public Integer getPresupuesto() {
		return presupuesto;
	}

	/**
	 * 
	 * @param presupuesto the presupuesto to set
	 */
	public void setPresupuesto(Integer presupuesto) {
		this.presupuesto = presupuesto;
	}


	/**
	 * 
	 * @return the empleado
	 */
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "empleado")
	public List<empleado> getEmpleado() {
		return empleado;
	}

	/**
	 * 
	 * @param empleado the empleado to set
	 */
	public void setEmpleado(List<empleado> empleado) {
		this.empleado = empleado;
	}


	//toString
	@Override
	public String toString() {
		return "departamento [id=" + id + ", nombre=" + nombre + ", presupuesto=" + presupuesto + "]";
	}

	
	
	
}
